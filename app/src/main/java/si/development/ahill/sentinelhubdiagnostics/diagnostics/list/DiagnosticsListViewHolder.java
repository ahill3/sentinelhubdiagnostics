package si.development.ahill.sentinelhubdiagnostics.diagnostics.list;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import si.development.ahill.sentinelhubdiagnostics.R;

class DiagnosticsListViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.diagnosticsItemImage)
    ImageView diagnosticsItemImage;

    @BindView(R.id.diagnosticsTitleText)
    TextView diagnosticsTitleText;

    @BindView(R.id.diagnosticsValueText)
    TextView diagnosticsValueText;

    DiagnosticsListViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
