package si.development.ahill.sentinelhubdiagnostics.diagnostics.list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import si.development.ahill.sentinelhubdiagnostics.R;
import si.development.ahill.sentinelhubdiagnostics.diagnostics.enums.DiagnosticsItemType;
import si.development.ahill.sentinelhubdiagnostics.diagnostics.utils.DiagnosticsUtil;

public class DiagnosticsListAdapter extends RecyclerView.Adapter<DiagnosticsListViewHolder> {

    private List<DiagnosticsListModel> data;

    public DiagnosticsListAdapter(List<DiagnosticsListModel> data) {
        this.data = data;
    }

    @Override
    public DiagnosticsListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_diagnostics, parent, false);
        return new DiagnosticsListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DiagnosticsListViewHolder holder, int position) {
        DiagnosticsListModel model = data.get(position);
        if (model == null) {
            return;
        }

        DiagnosticsItemType modelType = model.getType();
        if (model.getType() == null) {
            return;
        }

        holder.diagnosticsItemImage.setImageResource(modelType.getIconResource());
        holder.diagnosticsTitleText.setText(modelType.getTitle());
        holder.diagnosticsValueText.setText(DiagnosticsUtil.formatValue(model));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
