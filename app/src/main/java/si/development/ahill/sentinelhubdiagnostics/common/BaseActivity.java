package si.development.ahill.sentinelhubdiagnostics.common;

import android.app.Activity;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseActivity extends AppCompatActivity {

    //region Variables

    protected Unbinder unbinder;

    //endregion Variables

    //region Lifecycle Methods

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (unbinder != null) {
            unbinder.unbind();
        }
    }

    //endregion Lifecycle Methods

    //region Protected Methods

    /**
     * Initializes basic common activity functionalities.
     *
     * @param target      Activity context.
     * @param layoutResId Resource ID to be inflated.
     */
    protected void initializeActivity(Activity target, @LayoutRes int layoutResId) {
        setContentView(layoutResId);
        unbinder = ButterKnife.bind(target);
    }

    //endregion Protected Methods

}
