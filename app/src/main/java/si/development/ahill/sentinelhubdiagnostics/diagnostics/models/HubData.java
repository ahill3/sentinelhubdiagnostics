package si.development.ahill.sentinelhubdiagnostics.diagnostics.models;

public class HubData {
    private boolean valid;
    private String temperature;
    private String humidity;
    private String pressure;
    private String vBat;
    private String vInt;
    private String shunt;

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getPressure() {
        return pressure;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public String getvBat() {
        return vBat;
    }

    public void setvBat(String vBat) {
        this.vBat = vBat;
    }

    public String getvInt() {
        return vInt;
    }

    public void setvInt(String vInt) {
        this.vInt = vInt;
    }

    public String getShunt() {
        return shunt;
    }

    public void setShunt(String shunt) {
        this.shunt = shunt;
    }
}
