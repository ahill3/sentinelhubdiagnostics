package si.development.ahill.sentinelhubdiagnostics.diagnostics;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import butterknife.BindView;
import si.development.ahill.sentinelhubdiagnostics.R;
import si.development.ahill.sentinelhubdiagnostics.common.BaseActivity;
import si.development.ahill.sentinelhubdiagnostics.diagnostics.enums.DiagnosticsItemType;
import si.development.ahill.sentinelhubdiagnostics.diagnostics.enums.RssiLevel;
import si.development.ahill.sentinelhubdiagnostics.diagnostics.list.DiagnosticsListAdapter;
import si.development.ahill.sentinelhubdiagnostics.diagnostics.list.DiagnosticsListModel;
import si.development.ahill.sentinelhubdiagnostics.diagnostics.models.HubData;
import si.development.ahill.sentinelhubdiagnostics.diagnostics.utils.DiagnosticsUtil;
import si.development.ahill.sentinelhubdiagnostics.diagnostics.utils.ParsingUtil;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class DiagnosticsActivity extends BaseActivity {

    //region Variables

    private boolean hasUpdateRegistered = false;

    private BluetoothAdapter btAdapter;
    private BluetoothGatt btGatt;
    private BluetoothGattCharacteristic writeCharacteristic;
    private BluetoothLeScanner leScanner;
    private Handler handler;
    private List<ScanFilter> filters;
    private ScanSettings settings;

    //region Runnables

    private Runnable updateRunnable = new Runnable() {
        @Override
        public void run() {
            if (btGatt == null) {
                return;
            }
            btGatt.readRemoteRssi();
            btGatt.discoverServices();
            setUpdateProcess(true, true);
        }
    };

    //endregion Runnables

    //region Constants

    private static final int REQUEST_BLUETOOTH = 1;
    private static final int REQUEST_FINE_LOCATION = 2;
    private static final String DEVICE_ADDRESS = "E0:74:DE:5C:15:31";
    private static final String SERVICE_UUID = "11002001-B338-449D-AFDA-8868B96D118D";
    private static final String WRITE_UUID = "11002002-B338-449D-AFDA-8868B96D118D";
    private static final String READ_UUID = "11002003-B338-449D-AFDA-8868B96D118D";
    private static final long UPDATE_INTERVAL = 4000;
    private static final long SCAN_INTERVAL = 20000;
    private static final String TAG = "BluetoothActivity";

    //endregion Constants

    //region UI Elements

    @BindView(R.id.nameText)
    protected TextView nameText;

    @BindView(R.id.progressBar)
    protected ProgressBar progressBar;

    @BindView(R.id.diagnosticsRecycler)
    protected RecyclerView diagnosticsRecycler;

    //endregion UI Elements

    //region Bluetooth Callbacks

    private ScanCallback mScanCallback = new ScanCallback() {
        @Override
        @TargetApi(21)
        public void onScanResult(int callbackType, ScanResult result) {
            Log.d(TAG, "callbackType " + String.valueOf(callbackType));
            Log.d(TAG, "result " + result.toString());
            BluetoothDevice btDevice = result.getDevice();
            connectToDevice(btDevice);
            setDeviceName(btDevice);
        }

        @Override
        @TargetApi(21)
        public void onBatchScanResults(List<ScanResult> results) {
            for (ScanResult sr : results) {
                Log.d(TAG, "ScanResult " + sr.toString());
            }
        }

        @Override
        @TargetApi(21)
        public void onScanFailed(int errorCode) {
            Log.e(TAG, "Scan Failed - Error Code: " + errorCode);
        }
    };

    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {
                @Override
                public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.d(TAG, "onLeScan " + device.toString());
                            connectToDevice(device);
                        }
                    });
                }
            };

    private final BluetoothGattCallback gattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            Log.d(TAG, "onConnectionStateChange status: " + status);
            switch (newState) {
                case BluetoothProfile.STATE_CONNECTED:
                    Log.d(TAG, "gattCallback STATE_CONNECTED");
                    gatt.readRemoteRssi();
                    gatt.discoverServices();
                    setDeviceName(gatt.getDevice());
                    break;
                case BluetoothProfile.STATE_DISCONNECTED:
                    Log.d(TAG, "gattCallback STATE_DISCONNECTED");
                    break;
                default:
                    Log.d(TAG, "gattCallback STATE_OTHER");
            }

        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status != BluetoothGatt.GATT_SUCCESS) {
                Log.e(TAG, "onServicesDiscovered error");
                return;
            }

            List<BluetoothGattService> services = gatt.getServices();
            Log.d(TAG, "onServicesDiscovered " + services.toString());
            if (services.size() == 0) {
                Log.w(TAG, "No services detected");
                return;
            }

            BluetoothGattService service = gatt.getService(UUID.fromString(SERVICE_UUID));

            writeCharacteristic = service.getCharacteristic(UUID.fromString(WRITE_UUID));
            BluetoothGattCharacteristic readCharacteristic = service.getCharacteristic(UUID.fromString(READ_UUID));

            gatt.setCharacteristicNotification(readCharacteristic, true);

            BluetoothGattDescriptor descriptor = readCharacteristic.getDescriptors().get(0);
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            gatt.writeDescriptor(descriptor);
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);
            Log.d(TAG, "onCharacteristicWrite");

            BluetoothGattCharacteristic c = gatt
                    .getService(UUID.fromString(SERVICE_UUID))
                    .getCharacteristic(UUID.fromString(WRITE_UUID));
            gatt.readCharacteristic(c);
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorWrite(gatt, descriptor, status);
            Log.d(TAG, "onDescriptorWrite");

            if (writeCharacteristic != null) {
                String update = "update";
                writeCharacteristic.setValue(update);
                btGatt.writeCharacteristic(writeCharacteristic);
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
            Log.d(TAG, "onCharacteristicChanged " + characteristic.toString());
            setDeviceName(gatt.getDevice());
            String output = DiagnosticsUtil.bytesToHex(characteristic.getValue());
            final HubData data = DiagnosticsUtil.extractHubData(output);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (progressBar != null) {
                        progressBar.setVisibility(View.GONE);
                    }
                    initializeList(data);
                }
            });

            setUpdateProcess(true, !hasUpdateRegistered);
        }

        @Override
        public void onReadRemoteRssi(BluetoothGatt gatt, final int rssi, int status) {
            super.onReadRemoteRssi(gatt, rssi, status);
            Log.d(TAG, "RSSI: " + rssi);
            setSignalStrengthIndicator(DiagnosticsUtil.findRssiLevel(rssi));
        }
    };

    //endregion Bluetooth Callbacks

    //endregion Variables

    //region Activity Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeActivity(this, R.layout.activity_diagnostics);
        initializeBluetooth();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setDeviceName(null);
        setSignalStrengthIndicator(RssiLevel.NO_SIGNAL);
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            finish();
            return;
        }

        if (hasPermissions()) {
            if (Build.VERSION.SDK_INT >= 21) {
                leScanner = btAdapter.getBluetoothLeScanner();
                settings = new ScanSettings.Builder()
                        .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                        .build();
                filters = new ArrayList<>();
            }
            handler.post(new Runnable() {
                @Override
                public void run() {
                    scanLeDevice(true);
                }
            });
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (btAdapter != null && btAdapter.isEnabled() && leScanner != null) {
            scanLeDevice(false);
        }
        setUpdateProcess(false, true);
        if (btGatt != null) {
            btGatt.disconnect();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (btGatt == null) {
            return;
        }
        btGatt.close();
        btGatt = null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_FINE_LOCATION: {
                if (grantResults[0] == PERMISSION_GRANTED) {
                    Log.d(TAG, "Permission granted!");
                }
            }
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_BLUETOOTH) {
            if (resultCode == RESULT_CANCELED) {
                Log.d(TAG, "onActivityResult canceled");
                finish();
                return;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    //endregion Activity Methods

    //region Private Methods

    private void initializeList(HubData hubData) {
        if (!hubData.isValid()) {
            Toast.makeText(this, "Error while parsing data", Toast.LENGTH_SHORT).show();
            return;
        }
        diagnosticsRecycler.setLayoutManager(new LinearLayoutManager(this));
        diagnosticsRecycler.setHasFixedSize(true);
        List<DiagnosticsListModel> data = new ArrayList<>();
        data.add(new DiagnosticsListModel(DiagnosticsItemType.TEMPERATURE,
                ParsingUtil.translateToHumanReadable(hubData.getTemperature(), 0.1, 0.0)));
        data.add(new DiagnosticsListModel(DiagnosticsItemType.HUMIDITY,
                ParsingUtil.translateToHumanReadable(hubData.getHumidity(), 0.1, 9.0)));
        data.add(new DiagnosticsListModel(DiagnosticsItemType.PRESSURE,
                ParsingUtil.translateToHumanReadable(hubData.getPressure(), 0.1, 0.0)));
        data.add(new DiagnosticsListModel(DiagnosticsItemType.BATTERY,
                ParsingUtil.translateToHumanReadable(hubData.getvBat(), 1.0, 0.0)));
        data.add(new DiagnosticsListModel(DiagnosticsItemType.DC_INPUT,
                ParsingUtil.translateToHumanReadable(hubData.getvInt(), 0.01, 0.0)));
        data.add(new DiagnosticsListModel(DiagnosticsItemType.SHUNT,
                ParsingUtil.translateToHumanReadable(hubData.getShunt(), 0.1, 0.0)));
        diagnosticsRecycler.setAdapter(new DiagnosticsListAdapter(data));
    }

    private void setDeviceName(BluetoothDevice device) {
        if (device == null) {
            nameText.setText("");
            return;
        }
        String deviceName = device.getName();
        if (nameText.getText().length() == 0 && deviceName != null) {
            nameText.setText(deviceName);
        }
    }

    private void setSignalStrengthIndicator(RssiLevel level) {
        final int res = level.getDrawableResource();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                nameText.setCompoundDrawablesWithIntrinsicBounds(0, 0, res, 0);
            }
        });
    }

    private void setUpdateProcess(boolean activate, boolean update) {
        if (!update) {
            return;
        }
        if (activate) {
            handler.postDelayed(updateRunnable, UPDATE_INTERVAL);
        } else {
            handler.removeCallbacks(updateRunnable);
        }
        hasUpdateRegistered = activate;
    }

    //region Permission Methods

    private boolean hasPermissions() {
        if (btAdapter == null || !btAdapter.isEnabled()) {
            requestBluetoothEnable();
            return false;
        }
        if (!hasLocationPermissions()) {
            ActivityCompat.requestPermissions(this,
                    new String[]{ACCESS_FINE_LOCATION}, REQUEST_FINE_LOCATION);
            return false;
        }
        return true;
    }

    private void requestBluetoothEnable() {
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBtIntent, REQUEST_BLUETOOTH);
        Log.d(TAG, "Requested user enables Bluetooth. Try starting the scan again.");
    }

    private boolean hasLocationPermissions() {
        return ActivityCompat.checkSelfPermission(this,
                ACCESS_FINE_LOCATION) == PERMISSION_GRANTED;
    }

    //endregion Permission Methods

    //region Bluetooth Methods

    private void initializeBluetooth() {
        handler = new Handler();

        final BluetoothManager btManager = (BluetoothManager) getSystemService(BLUETOOTH_SERVICE);
        if (btManager == null) {
            finish();
            return;
        }
        btAdapter = btManager.getAdapter();
    }

    private void scanLeDevice(final boolean enable) {
        if (enable) {
            if (diagnosticsRecycler != null) {
                diagnosticsRecycler.setAdapter(null);
            }
            if (progressBar != null) {
                progressBar.setVisibility(View.VISIBLE);
            }
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (Build.VERSION.SDK_INT < 21) {
                        Log.d(TAG, "Handler: Stopping LE Scan");
                        btAdapter.stopLeScan(mLeScanCallback);
                    } else {
                        Log.d(TAG, "Handler: Stopping Normal Scan");
                        leScanner.stopScan(mScanCallback);
                    }
                }
            }, SCAN_INTERVAL);
            if (Build.VERSION.SDK_INT < 21) {
                Log.d(TAG, "Starting LE Scan");
                btAdapter.startLeScan(mLeScanCallback);
            } else {
                Log.d(TAG, "Starting Normal Scan");
                leScanner.startScan(filters, settings, mScanCallback);
            }
        } else {
            if (Build.VERSION.SDK_INT < 21) {
                Log.d(TAG, "Stopping LE Scan");
                btAdapter.stopLeScan(mLeScanCallback);
            } else {
                Log.d(TAG, "Stopping Normal Scan");
                leScanner.stopScan(mScanCallback);
            }
        }
    }

    private void connectToDevice(BluetoothDevice device) {
        if (btGatt != null) {
            btGatt.disconnect();
        }
        if (device.getAddress().equals(DEVICE_ADDRESS)) {
            btGatt = device.connectGatt(this, false, gattCallback);
            scanLeDevice(false);
        }
    }

    //endregion Bluetooth Methods

    //endregion Private Methods

}
