package si.development.ahill.sentinelhubdiagnostics.diagnostics.enums;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

import si.development.ahill.sentinelhubdiagnostics.R;

public enum DiagnosticsItemType {

    TEMPERATURE(R.string.diagnostics_type_temperature, R.drawable.ic_temperature, "°C"),
    HUMIDITY(R.string.diagnostics_type_humidity, R.drawable.ic_humidity, "%"),
    PRESSURE(R.string.diagnostics_type_pressure_scale, R.drawable.ic_pressure_scale, "mBar"),
    BATTERY(R.string.diagnostics_type_battery, R.drawable.ic_battery, "%"),
    DC_INPUT(R.string.diagnostics_type_dc_input, R.drawable.ic_dc_input, "V"),
    SHUNT(R.string.diagnostics_type_shunt, R.drawable.ic_shunt, "mV");

    @StringRes
    private int title;
    @DrawableRes
    private int iconResource;
    private String unit;

    DiagnosticsItemType(@StringRes int title, @DrawableRes int iconResource, String unit) {
        this.title = title;
        this.iconResource = iconResource;
        this.unit = unit;
    }

    @StringRes
    public int getTitle() {
        return title;
    }

    @DrawableRes
    public int getIconResource() {
        return iconResource;
    }

    public String getUnit() {
        return unit;
    }
}
