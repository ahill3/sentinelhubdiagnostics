package si.development.ahill.sentinelhubdiagnostics.diagnostics.list;

import si.development.ahill.sentinelhubdiagnostics.diagnostics.enums.DiagnosticsItemType;

public class DiagnosticsListModel {

    private DiagnosticsItemType type;
    private Double value;

    public DiagnosticsListModel(DiagnosticsItemType type, Double value) {
        this.type = type;
        this.value = value;
    }

    public DiagnosticsItemType getType() {
        return type;
    }

    public Double getValue() {
        return value;
    }
}
