package si.development.ahill.sentinelhubdiagnostics.diagnostics.enums;

import android.support.annotation.DrawableRes;

import si.development.ahill.sentinelhubdiagnostics.R;

public enum RssiLevel {
    GREAT_SIGNAL(-55, Integer.MAX_VALUE, R.drawable.ic_great_signal),
    GOOD_SIGNAL(-66, -56, R.drawable.ic_good_signal),
    FAIR_SIGNAL(-77, -67, R.drawable.ic_fair_signal),
    BAD_SIGNAL(-88, -78, R.drawable.ic_bad_signal),
    NO_SIGNAL(Integer.MIN_VALUE, -89, R.drawable.ic_no_signal);

    private int from;
    private int to;
    @DrawableRes
    int drawableResource;

    RssiLevel(int from, int to, @DrawableRes int drawableResource) {
        this.from = from;
        this.to = to;
        this.drawableResource = drawableResource;
    }

    public int getFrom() {
        return from;
    }

    public int getTo() {
        return to;
    }

    @DrawableRes
    public int getDrawableResource() {
        return drawableResource;
    }
}
