package si.development.ahill.sentinelhubdiagnostics.diagnostics.utils;

public class ParsingUtil {

    private static Double parseData(String hex) {
        try {
            Long l = Long.parseLong(hex, 16);
            return l.doubleValue();
        } catch (NumberFormatException | NullPointerException e) {
            return null;
        }
    }

    public static Double translateToHumanReadable(String hex, Double factor, Double extra) {
        Double data = parseData(hex);
        if (data == null) {
            return null;
        }
        return (data + extra) * factor;
    }

}
