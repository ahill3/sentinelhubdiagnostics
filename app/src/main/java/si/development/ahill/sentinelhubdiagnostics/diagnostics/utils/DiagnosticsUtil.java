package si.development.ahill.sentinelhubdiagnostics.diagnostics.utils;

import java.util.Locale;

import si.development.ahill.sentinelhubdiagnostics.diagnostics.enums.DiagnosticsItemType;
import si.development.ahill.sentinelhubdiagnostics.diagnostics.enums.RssiLevel;
import si.development.ahill.sentinelhubdiagnostics.diagnostics.list.DiagnosticsListModel;
import si.development.ahill.sentinelhubdiagnostics.diagnostics.models.HubData;

public class DiagnosticsUtil {

    private static final char[] hexArray = "0123456789ABCDEF".toCharArray();

    public static String formatValue(DiagnosticsListModel model) {
        Double value = model.getValue();
        DiagnosticsItemType type = model.getType();
        return String.format(Locale.getDefault(), "%.1f %s", value, type.getUnit());
    }

    public static HubData extractHubData(String hubOutput) {
        HubData data = new HubData();
        data.setValid(hubOutput.startsWith("61") &&
                hubOutput.substring(2, 4).equals("00") &&
                hubOutput.substring(16, 18).equals("01") &&
                hubOutput.substring(22, 24).equals("02") &&
                hubOutput.substring(28, 30).equals("03"));

        if (data.isValid()) {
            data.setTemperature(DiagnosticsUtil.convertToBigEndian(hubOutput.substring(4, 8)));
            data.setHumidity(DiagnosticsUtil.convertToBigEndian(hubOutput.substring(8, 12)));
            data.setPressure(DiagnosticsUtil.convertToBigEndian(hubOutput.substring(12, 16)));
            data.setvBat(DiagnosticsUtil.convertToBigEndian(hubOutput.substring(18, 22)));
            data.setvInt(DiagnosticsUtil.convertToBigEndian(hubOutput.substring(24, 28)));
            data.setShunt(DiagnosticsUtil.convertToBigEndian(hubOutput.substring(30, 34)));
        }

        return data;
    }

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    private static String convertToBigEndian(String hex) {
        int lengthInBytes = hex.length() / 2;
        char[] chars = new char[lengthInBytes * 2];
        for (int index = 0; index < lengthInBytes; index++) {
            int reversedIndex = lengthInBytes - 1 - index;
            chars[reversedIndex * 2] = hex.charAt(index * 2);
            chars[reversedIndex * 2 + 1] = hex.charAt(index * 2 + 1);
        }
        return new String(chars);
    }

    public static RssiLevel findRssiLevel(int rssiValue) {
        for (RssiLevel rssiLevel : RssiLevel.values()) {
            if (rssiValue >= rssiLevel.getFrom() && rssiValue <= rssiLevel.getTo()) {
                return rssiLevel;
            }
        }
        return RssiLevel.NO_SIGNAL;
    }
}
